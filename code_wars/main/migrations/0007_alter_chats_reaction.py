# Generated by Django 5.0.1 on 2024-02-13 16:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_user_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chats',
            name='Reaction',
            field=models.TextField(blank=True, choices=[('love', '❤️'), ('smile', '😂'), ('Robot', '🦾'), ('skull', '💀'), ('celebrate', '🥳'), ('bat', '🏏')]),
        ),
    ]
