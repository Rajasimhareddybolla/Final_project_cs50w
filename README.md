# Code-Colab-Hub  
## Code-Wars 

Welcome to ***Code-Wars*** which is a open source project which is open for *Commits,Fork* or can gave sugesttions

Code-Wars is a web-application where clients can Solve some good Quality of problems based on their *intrests* .Hear they can collaborate with each other make *Friends,Groups and solve problems* .
 ## 🛠 Tecnologys used
 * Javascript, CSS, HTML  
 * Python 3.12.1  
 * Django 5.0.2  
 * Boot strap 5.0
 * SQLITE 

## Run Locally

Clone the project

```bash
  git clone https://github.com/Rajasimhareddybolla/Final_project_cs50w.git
```

Go to the project directory

```bash
  cd code_wars
```

Install dependencies

```bash
  pip install -r requirements. txt 
```
Activate Database  
```bash
    Python manage.py makemigrations
    python manage.py migrate
```

Start the server

```bash
  python manage.py runserver
```

![execution](code_wars/reference_section/icons/installation.png/installation.png)

## Features <a id="2"></a>
- Solve problem on Daily Basis
- Collaborate with firends
- Make groups 
- Share Your Ideas
- Cross platform
- Responsive design
- User Friendly
## Scope for Contribution
> Can Contribute through redusing code redundence.  
> Asyncronous Chat and Connections  
> Live challenge with real time Users.  
>  Can modify Front end  
> Adding Live Validation of Answers  
> Code optimisation




## <a id="chapter-1"></a> Documentation

[Documentation](code_wars/reference_section/documentation.md)  
Refer this section for Documentation of this project for code reference

#### Things I learnt during this project

They are many problems which I tackeld on completion of this project from Primery Design to Deployment which i documented [hear](/code_wars/reference_section/things_i_learnt.md)

#### Basic_design of this project
[Figma Design](https://www.figma.com/file/oUbIeTRjEkV49fJMhmxMEY/Untitled?type=design&node-id=0%3A1&mode=dev&t=B5z1c2mCc40TANYx-1)
## Contributors

Contributions are always welcome!


Please adhere to this project's `code of conduct`.

## Distinctiveness and Complexity
Hear this section dedicated to CS50 check authorites as mentioned this project is not on the following projects 
> 1) commerce (Not at all related to that)  
> 2) Socila networking (I implemented a feauter to connect or msg with each other its just a feauter )(which not resemble whole project)

As i mentione above the feauters of this [project](#2) we can state that the uniqueness of this project and how it is complex than other 



plz refer to this doc for more imformation about the project / files  / how it works

Refer to [Documentation](#chapter-1)